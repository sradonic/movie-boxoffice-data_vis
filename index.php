<?php include_once('header.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" type="text/css" href ="css/index.css">
<script>
function showDiv(){
  document.getElementById("buttons").style.display = "block";
}
</script>
</head>
<body>
<div class="row">
  <div class="col-md-6">
    <div id="druga_sl">
      <a href="map.php" class="thumbnail">
        <img src="img/maps.png" alt="...">
      </a>
    </div>
  </div>
  <div class="col-md-6">
    <div id="prva_sl">
      <a href="#" class="thumbnail">
        <img onclick="showDiv()" src="img/bar_charts.png" alt="...">
      </a>
      <div id="button_container" class="col-md-12">
        <div id="buttons">
          <a href="search.php" class="btn btn-default btn-lg active" role="button">Search</a>
          <a href="bars.php" class="btn btn-default btn-lg active" role="button">Predefined</a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
