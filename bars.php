<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <script src="//d3js.org/d3.v3.min.js"></script>
  <script src="http://labratrevenge.com/d3-tip/javascripts/d3.tip.v0.6.3.js"></script>
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<script src="js/bars.js"></script>
<link rel="stylesheet" type="text/css" href ="css/bars.css">

</head>
<body>
  <?php include_once('header.php'); ?>

<div id="containForm">
<form name="myForm" class="form-inline">
    <label for="exampleInputName2">From</label>
    <input name="fromform" type="text" class="form-control" id="fromform" placeholder="10">
    <label for="exampleInputEmail2">To</label>
    <input type="toform" class="form-control" id="toform" placeholder="20">
  <input id="a" type="button" class="btn btn-default" value="Draw" onclick="myFunction()">
</form>
</div>

<div id="contain">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">2015</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2015" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2015" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2015" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">2014</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2014" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2014" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2014" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">2013</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2013" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2013" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2013" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">2012</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2012" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2012" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="21+012" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">2011</a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2011" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2011" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2011" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">2010</a>
        </h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2010" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2010" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2010" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">2009</a>
        </h4>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2009" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2009" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2009" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">2008</a>
        </h4>
      </div>
      <div id="collapse8" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2008" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2008" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2008" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">2007</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2007" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2007" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2007" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">2006</a>
        </h4>
      </div>
      <div id="collapse10" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2006" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2006" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2006" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">2005</a>
        </h4>
      </div>
      <div id="collapse11" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2005" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2005" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2005" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">2004</a>
        </h4>
      </div>
      <div id="collapse12" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2004" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2004" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2004" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">2003</a>
        </h4>
      </div>
      <div id="collapse13" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2003" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2003" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2003" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">2002</a>
        </h4>
      </div>
      <div id="collapse14" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2002" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2002" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2002" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">2001</a>
        </h4>
      </div>
      <div id="collapse15" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2001" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2001" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2001" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse16">2000</a>
        </h4>
      </div>
      <div id="collapse16" class="panel-collapse collapse">
        <div class="list-group">
          <a href="#" onclick="showGraph10(this);" id="2000" class="list-group-item">Top 10</a>
          <a href="#" onclick="showGraph20(this);" id="2000" class="list-group-item">Top 20</a>
          <a href="#" onclick="showGraphXY(this);" id="2000" class="list-group-item">Top XY</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="chart"></div>
<div id="pie"></div>


</body>
</html>
