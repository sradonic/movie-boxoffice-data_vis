$(function() {	
	var width = document.getElementById('map-container').offsetWidth-10;
	var height = width / 2;
	var circleColorActive = "#0B1212";

	var infoDiv = d3.select("body")
	.append("div")
	.style("opacity", "0")
	.style("transition", "0.1s linear")
	.style("position", "absolute")
	.style("width", "auto")
	.style("height", "auto")
	.style("background", "black")
	.style("border", "1px solid #777")
	.style("border-radius", "3px")
	.style("text-align", "center")
	.style("font-family", "Roboto")
	.style("font-size", "18px")
	.style("font-weight", "600")
	.style("color", "white")
	.style("padding", "10px");


	var zoom = d3.behavior.zoom()
    	.scaleExtent([1, 8])
    	.on("zoom", move);			

  	var projection = d3.geo.mercator()
    	.translate([0, 60])
    	.scale(width / 2 / Math.PI);

	var path = d3.geo.path()
		.projection(projection);	

	var svg = d3.select("#map-container").append("svg")
      	.attr("width", width)
      	.attr("height", height)
      	.append("g")
      	.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
      	.call(zoom);	

    var div = d3.select("body").append("div") 
    .attr("class", "tooltip")       
    .style("opacity", 0);

    var g = svg.append("g");

    drawMap();

    function drawMap() {
		d3.json("map/world-topo.json", function(error, world){
			if(error) throw error;
			var countries = topojson.feature(world, world.objects.countries).features;
		
			var country = g.selectAll(".country").data(countries);
			//console.log(country);			

			country.enter().insert("path")	      
		      .attr("d", path)
		      .attr("stroke", "#555")
		      .attr("stroke-width", 1)
		      .attr("id", function(d,i) { return d.id; })
		      .attr("title", function(d,i) { return d.properties.name; })
		      .style("fill", "#999")
		      .attr("class", function(d) { return "country"+d.properties.name + " country";});

		    var offsetL = document.getElementById('map-container').offsetLeft+(width/2)+40;
			var offsetT =document.getElementById('map-container').offsetTop+(height/2)+20;
		 });	
	}

	function move() {
		var t = d3.event.translate;
		var s = d3.event.scale;  
		var h = height / 3;

		t[0] = Math.min(width / 2 * (s - 1), Math.max(width / 2 * (1 - s), t[0]));
		t[1] = Math.min(height / 2 * (s - 1) + h * s, Math.max(height / 2 * (1 - s) - h * s, t[1]));

		zoom.translate(t);
		g.style("stroke-width", 1 / s).attr("transform", "translate(" + t + ")scale(" + s + ")");
	}

	$.getJSON("json/boxofficeByCountry.json", function(data){ //dohvacanje podataka zarade filmova po zemlji
		//console.log(data.movies[0]);
		for (var i = 0 ; i < data.movies[0].movie.length ; i++) //prolazak kroz sve filmove te zabiljezavanje svih filmova u movie varijablu
        {
        	var movie = data.movies[0].movie[i]; //ima sve podatke o filmovima u sebi
        //	console.log(i);
        //	console.log(movie);

        	selectv(movie); //slanje filmova u funkciju selectv
        	var div_data="<option>"+movie.name+"</option>"; //popunjava drop-down menu
       // alert(div_data);
        	$(div_data).appendTo('#dd-menu'); 
        }  
        var movie = data.movies[0];
        //console.log(movie);
        //selectv(movie);
	});

	
	function selectv(movie) { //funkcija koja uzima value (film) iz drop-down menu-a te provjerava za koje zemlje on ima podatke		
		$(function() {
			$("#dd-menu").on("change",function() {
				var select = this.value;
				if (select==movie.name) 
				{ 
					reset();
					var selectedCountry = [];
					var boxoffice = [];
					var distributor = [];
					var releaseDate = [];
					var openingWknd = [];
					var percentTotal = [];
					var boxoffices = [];
					for (var i=0; i<movie.countries.length;i++)
					{
						selectedCountry.push(movie.countries[i].Country);
						boxoffice.push(parseFloat((movie.countries[i].Boxoffice).split(",").join("")));
						distributor.push(movie.countries[i].Dist);
						releaseDate.push(movie.countries[i].ReleaseDate);
						openingWknd.push(movie.countries[i].OpeningWknd);
						percentTotal.push(movie.countries[i].PercentOfTotal);
						boxoffices.push(movie.countries[i].Boxoffice);
					}
					connectSelectCountry(selectedCountry, boxoffice, distributor, select, releaseDate, openingWknd, percentTotal, boxoffices);
				}
			});
		});
	}


	function reset(){
		//console.log("reset_main");
		g.selectAll(".country")
			.style("fill", "#999")
			.on('mouseover', function(d){return false; })
			.on("mouseout", function(d){return false; })
			.style("opacity", "1")
			.on('click', function(d){ return false; });		     			
	}

	function connectSelectCountry(selectedCountry, boxoffice, distributor, select, releaseDate, openingWknd ,percentTotal, boxoffices) {
		//console.log(boxoffice);
		var colorScale = d3.scale.linear().range([0.2,1.0]).domain([d3.min(boxoffice), d3.max(boxoffice)]);			
		for (var i=0;i<selectedCountry.length;i++)
        {
			var currentIndex = i;
			(function(i) {
            g.selectAll(".country" + selectedCountry[i])
                .on('click', function(d){
                var active = d.active ? false:true,
                newOpacity = active ? 0.9:0;
                div .transition()
                    .duration(200)
                    .style("opacity", newOpacity);
                div .html(select +"<br>"+ "Distributor: " + distributor[i] + "<br>" + "Release Date: " + releaseDate[i] + "<br>" + "Opening Weekend: " +"$"+ openingWknd[i] + "<br>" +"Percent of Total: " +percentTotal[i] );
                d.active = active;
                 })
		       		.on("mouseover", function(d){				
	       			infoDiv
					.style("left", (d3.event.pageX - 50))
					.style("top", (d3.event.pageY + 50))
					.style("opacity", "0.8")
					.html("$" + boxoffices[i] +"<br>"+ selectedCountry[i]);
						})
		       		.on("mouseout", function(d){
		       			infoDiv
		       			 .style("opacity", "0");
		       		})
            .style("fill", "#2980b9")
            .style("cursor", "pointer")
            .style("opacity", colorScale(boxoffice[i]));     
		}(currentIndex));      
        }
	}

	$("#dd-menu").on("click",function() {
		console.log("menuClick");
		$('.tooltip').css({"opacity": "0"});
	});
	$("#map-container").on("click",function() {
		console.log("mapClick");
		$('.tooltip').css({"opacity": "0"});
	});

}); 	

