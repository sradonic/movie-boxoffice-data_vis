function showGraph10(year){
    
    $('div#chart').empty();   
    $('div#pie').empty();

    document.getElementById("chart").style.visibility = "visible";
    document.getElementById("containForm").style.visibility = "hidden";
    document.getElementById("pie").style.marginTop = "-250px";
var margin ={top:20, right:30, bottom:68, left:40},
    width=800-margin.left - margin.right, 
    height=650-margin.top-margin.bottom;

// scale to ordinal because x axis is not numerical
var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);

//scale to numerical value by height
var y = d3.scale.linear().range([height, 0]);

var chart = d3.select("#chart")  
              .append("svg")  //append svg element inside #chart
              .attr("width", width+(2*margin.left)+margin.right)    //set width
              .attr("height", height+margin.top+margin.bottom);  //set height

var xAxis = d3.svg.axis()
              .scale(x)
              .orient("bottom");  //orient bottom because x-axis will appear below the bars

var yAxis = d3.svg.axis()
              .scale(y)
              .orient("left");

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>"+d.Title+"</strong> <span style='color:orange'>"+"$"+d.Worldwide+"M"+"</span>";
  });

  var div = d3.select("body").append("div") 
    .attr("class", "tooltip")       
    .style("opacity", 0);

  var canvasWidth = 800, 
      canvasHeight = 800,   
      outerRadius = 200,   
      color = d3.scale.category20(); 

      var legendRectSize = 18;
      var legendSpacing = 4;

      var div = d3.select("body").append("div")   
                  .attr("class", "tooltip1")               
                  .style("opacity", 0);

var tip1 = d3.tip()
  .attr('class', 'd3-tip1')
  .offset([70, 0])
  .html(function(d) {
    console.log(d);
    return "<strong>"+d.data.Title+"</strong> <span style='color:orange'>"+"$"+d.data.Worldwide+"M"+"</span>";
  });

  d3.json("json/boxoffice"+year.id+"_2mov.json", function(error, data){

  data.movies = data.movies.slice(0, 10); // za 10 najboljih

  x.domain(data.movies.map(function(d){ return d.Title}));
  y.domain([0, d3.max(data.movies, function(d){return d.Worldwide*1.05})]);

  console.log(data.movies)
  var bar = chart.selectAll("g")
            .data(data.movies)
          .enter()
            .append("g")
            .attr("transform", function(d, i){
              return "translate("+x(d.Title)+", 0)";
            })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)
            .on('click', function(d){
                var active = d.active ? false:true,
                newOpacity = active ? 0.95:0;
                var MovieName = d.Title;
                var MovieWorldwide = d.Worldwide;
                var MovieDomestic = d.Domestic;
                var MovieOverseas = d.Overseas;
                var MovieRank = d.Rank;
                var MovieStudio = d.Studio;
                var MovieDomesticPer=d.Dper;
                var MovieOverseasPer=d.Oper;
                div .transition()
                    .duration(200)
                    .style("opacity", newOpacity);
                div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                d.active = active;
            });
  
  bar.call(tip);
  bar.append("rect")
      .attr("y", function(d) { 
        return y(d.Worldwide); 
      })
      .attr("x", function(d,i){
        return x.rangeBand()+(margin.left/2);
      })
      .attr("height", function(d) { 
        return height - y(d.Worldwide); 
      })
      .attr("width", x.rangeBand()*0.7) 
      .attr("transform", "translate(-35, 0)")

  bar.append("text")
      .attr("x", x.rangeBand()+margin.left )
      .attr("y", function(d) { return y(d.Worldwide) -10; })
      .attr("dy", ".75em");
  
  chart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate("+margin.left+","+ height+")")        
        .call(xAxis);
  
  chart.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate("+margin.left+",0)")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Boxoffice");

  d3.selectAll("g.x.axis g.tick line")
    .attr("y2", 25)
    .attr("x2", 0);   

/*pie chart */
    var vis = d3.select("#pie")
      .append("svg:svg") 
        .data([data.movies]) 
        .attr("width", canvasWidth) 
        .attr("height", canvasHeight) 
        .append("svg:g") 
          .attr("transform", "translate(" + 2.5*outerRadius + "," + 1.5*outerRadius + ")") 
vis.call(tip1);
    var arc = d3.svg.arc()
      .outerRadius(outerRadius);

    var pie = d3.layout.pie() 
      .value(function(d) { return d.Worldwide; }) 
      .sort( function(d) { return null; } );

    var arcs = vis.selectAll("g.slice")
      .data(pie)
      .enter()
      .append("svg:g")
      .attr("class", "slice");  

    arcs.append("svg:path")
      .attr("fill", function(d, i) { return color(i); } )
      .attr("d", arc)
      .on('mouseover', tip1.show)
      .on('mouseout', tip1.hide)
      .on('click', function(d){
                        var active = d.active ? false:true,
                        newOpacity = active ? 0.95:0;
                        var MovieName = d.data.Title;
                        var MovieWorldwide = d.data.Worldwide;
                        var MovieDomestic = d.data.Domestic;
                        var MovieOverseas = d.data.Overseas;
                        var MovieRank = d.data.Rank;
                        var MovieStudio = d.data.Studio;
                        var MovieDomesticPer=d.data.Dper;
                        var MovieOverseasPer=d.data.Oper;
                        div .transition()
                            .duration(200)
                            .style("opacity", newOpacity);
                        div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                        d.active = active;
                    });

  /*  arcs.append("svg:text")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius + 50; // Set Outer Coordinate
        d.innerRadius = outerRadius + 50; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" +angle(d) + ")";
      })
      .attr("text-anchor", "middle") 
      .style("fill", "Purple")
      .style("font", "bold 12px Arial");
      .attr("data-legend",function(d) { return d.data.Title; })
      .text(function(d, i) { return data.movies[i].Title; }); */

    arcs.filter(function(d) { return d.endAngle - d.startAngle > .2; }).append("svg:text")
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius; // Set Outer Coordinate
        d.innerRadius = outerRadius/2; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")";
      })
      .style("fill", "White")
      .style("font", "bold 12px Arial")
      .text(function(d) { return d.data.Worldwide; });

    

    var legend = vis.selectAll('.legend')
      .data(color.domain())
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', function(d, i) {
        var height = legendRectSize + legendSpacing;
        var offset =  height * color.domain().length / 2;
        var horz = -2 * legendRectSize-400;
        var vert = i * height - offset ;
        return 'translate(' + horz + ',' + vert + ')';
      });

    legend.append('rect')
    .attr('width', legendRectSize)
    .attr('height', legendRectSize)
    .style('fill', color)
    .style('stroke', color);

    legend.append('text')
      .attr('x', legendRectSize + legendSpacing)
      .attr('y', legendRectSize - legendSpacing)
      .text(function(d, i) { return data.movies[i].Title; });

    function angle(d) {
      var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
      return a > 90 ? a - 180 : a;
    } 
});

function type(d) {
    d.Title = +d.Title; // coerce to number
    return d;
  }
}


function showGraph20(year){
    $('div#chart').empty();
    $('div#pie').empty();
    document.getElementById("chart").style.visibility = "visible";
    document.getElementById("containForm").style.visibility = "hidden";
    document.getElementById("pie").style.marginTop = "-150px";
var margin ={top:20, right:32, bottom:70, left:40},
    width=1000-margin.left - margin.right, 
    height=760-margin.top-margin.bottom;

// scale to ordinal because x axis is not numerical
var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);

//scale to numerical value by height
var y = d3.scale.linear().range([height, 0]);

var chart = d3.select("#chart")  
              .append("svg")  //append svg element inside #chart
              .attr("width", width+(2*margin.left)+margin.right)    //set width
              .attr("height", height+margin.top+margin.bottom);  //set height

var xAxis = d3.svg.axis()
              .scale(x)
              .orient("bottom");  //orient bottom because x-axis will appear below the bars

var yAxis = d3.svg.axis()
              .scale(y)
              .orient("left");

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>"+d.Title+"</strong> <span style='color:orange'>"+"$"+d.Worldwide+"M"+"</span>";
  });

  var div = d3.select("body").append("div") 
    .attr("class", "tooltip")       
    .style("opacity", 0);

  var canvasWidth = 800, 
      canvasHeight = 800,   
      outerRadius = 200,   
      color = d3.scale.category20(); 

      var legendRectSize = 18;
      var legendSpacing = 4;

      var div = d3.select("body").append("div")   
                  .attr("class", "tooltip1")               
                  .style("opacity", 0);

var tip1 = d3.tip()
  .attr('class', 'd3-tip1')
  .offset([70, 0])
  .html(function(d) {
    console.log(d);
    return "<strong>"+d.data.Title+"</strong> <span style='color:orange'>"+"$"+d.data.Worldwide+"M"+"</span>";
  });

  d3.json("json/boxoffice"+year.id+"_2mov.json", function(error, data){

  data.movies = data.movies.slice(0, 20); // za 20 najboljih

  x.domain(data.movies.map(function(d){ return d.Title}));
  y.domain([0, d3.max(data.movies, function(d){return d.Worldwide*1.05})]);

  console.log(data)
  var bar = chart.selectAll("g")
                    .data(data.movies)
                  .enter()
                    .append("g")
                    .attr("transform", function(d, i){
                      return "translate("+x(d.Title)+", 0)";
                    })
                    .on('mouseover', tip.show)
                    .on('mouseout', tip.hide)
                    .on('click', function(d){
                        var active = d.active ? false:true,
                        newOpacity = active ? 0.95:0;
                        var MovieName = d.Title;
                        var MovieWorldwide = d.Worldwide;
                        var MovieDomestic = d.Domestic;
                        var MovieOverseas = d.Overseas;
                        var MovieRank = d.Rank;
                        var MovieStudio = d.Studio;
                        var MovieDomesticPer=d.Dper;
                        var MovieOverseasPer=d.Oper;
                        div .transition()
                            .duration(200)
                            .style("opacity", newOpacity);
                        div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                        d.active = active;
                    });
  
  bar.call(tip);
  bar.append("rect")
      .attr("y", function(d) { 
        return y(d.Worldwide); 
      })
      .attr("x", function(d,i){
        return x.rangeBand()+(margin.left/2);
      })
      .attr("height", function(d) { 
        return height - y(d.Worldwide); 
      })
      .attr("width", x.rangeBand()*0.7)
      .attr("transform", "translate(-13, 0)");

  bar.append("text")
      .attr("x", x.rangeBand()+margin.left )
      .attr("y", function(d) { return y(d.Worldwide) -10; })
      .attr("dy", ".75em");
  
  chart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate("+margin.left+","+ height+")")        
        .call(xAxis);
  
  chart.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate("+margin.left+",0)")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Boxoffice");

           d3.selectAll("g.x.axis g.tick line")
    .attr("y2", 20)
    .attr("x2", 0); 
/*pie chart */
    var vis = d3.select("#pie")
      .append("svg:svg") 
        .data([data.movies]) 
        .attr("width", canvasWidth) 
        .attr("height", canvasHeight) 
        .append("svg:g") 
          .attr("transform", "translate(" + 2.5*outerRadius + "," + 1.5*outerRadius + ")") 
vis.call(tip1);
    var arc = d3.svg.arc()
      .outerRadius(outerRadius);

    var pie = d3.layout.pie() 
      .value(function(d) { return d.Worldwide; }) 
      .sort( function(d) { return null; } );

    var arcs = vis.selectAll("g.slice")
      .data(pie)
      .enter()
      .append("svg:g")
      .attr("class", "slice");  

    arcs.append("svg:path")
      .attr("fill", function(d, i) { return color(i); } )
      .attr("d", arc)
      .on('mouseover', tip1.show)
      .on('mouseout', tip1.hide)
      .on('click', function(d){
                        var active = d.active ? false:true,
                        newOpacity = active ? 0.95:0;
                        var MovieName = d.data.Title;
                        var MovieWorldwide = d.data.Worldwide;
                        var MovieDomestic = d.data.Domestic;
                        var MovieOverseas = d.data.Overseas;
                        var MovieRank = d.data.Rank;
                        var MovieStudio = d.data.Studio;
                        var MovieDomesticPer=d.data.Dper;
                        var MovieOverseasPer=d.data.Oper;
                        div .transition()
                            .duration(200)
                            .style("opacity", newOpacity);
                        div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                        d.active = active;
                    });

  /*  arcs.append("svg:text")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius + 50; // Set Outer Coordinate
        d.innerRadius = outerRadius + 50; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" +angle(d) + ")";
      })
      .attr("text-anchor", "middle") 
      .style("fill", "Purple")
      .style("font", "bold 12px Arial");
      .attr("data-legend",function(d) { return d.data.Title; })
      .text(function(d, i) { return data.movies[i].Title; }); */

    arcs.filter(function(d) { return d.endAngle - d.startAngle > .2; }).append("svg:text")
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius; // Set Outer Coordinate
        d.innerRadius = outerRadius/2; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")";
      })
      .style("fill", "White")
      .style("font", "bold 12px Arial")
      .text(function(d) { return d.data.Worldwide; });

    

    var legend = vis.selectAll('.legend')
      .data(color.domain())
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', function(d, i) {
        var height = legendRectSize + legendSpacing;
        var offset =  height * color.domain().length / 2;
        var horz = -2 * legendRectSize-400;
        var vert = i * height - offset ;
        return 'translate(' + horz + ',' + vert + ')';
      });

    legend.append('rect')
    .attr('width', legendRectSize)
    .attr('height', legendRectSize)
    .style('fill', color)
    .style('stroke', color);

    legend.append('text')
      .attr('x', legendRectSize + legendSpacing)
      .attr('y', legendRectSize - legendSpacing)
      .text(function(d, i) { return data.movies[i].Title; });

    function angle(d) {
      var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
      return a > 90 ? a - 180 : a;
    } 
});

function type(d) {
    d.Title = +d.Title; // coerce to number
    return d;
  }
}

function showGraphXY(year){
document.getElementById("containForm").style.visibility = "visible";
    $('div#chart').empty();   
    $('div#pie').empty();
    var iDyear=year.id;
document.getElementById("a").onclick = function() { myFunction(iDyear);}
}
var from = 0;
var to=10;

function myFunction(iDyear){
  document.getElementById("containForm").style.visibility = "hidden";
  var newFrom = document.getElementById('fromform').value;
  var newTo = document.getElementById('toform').value;
  var from = newFrom;
  var to= newTo;
  console.log(from);
  if(to-from == 10)
  {
    $('div#chart').empty();   
    $('div#pie').empty();

    document.getElementById("chart").style.visibility = "visible";
 var margin ={top:20, right:30, bottom:68, left:40},
    width=800-margin.left - margin.right, 
    height=755-margin.top-margin.bottom;

// scale to ordinal because x axis is not numerical
var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);

//scale to numerical value by height
var y = d3.scale.linear().range([height, 0]);

var chart = d3.select("#chart")  
              .append("svg")  //append svg element inside #chart
              .attr("width", width+(2*margin.left)+margin.right)    //set width
              .attr("height", height+margin.top+margin.bottom);  //set height

var xAxis = d3.svg.axis()
              .scale(x)
              .orient("bottom");  //orient bottom because x-axis will appear below the bars

var yAxis = d3.svg.axis()
              .scale(y)
              .orient("left");

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>"+d.Title+"</strong> <span style='color:orange'>"+"$"+d.Worldwide+"M"+"</span>";
  });

  var div = d3.select("body").append("div") 
    .attr("class", "tooltip")       
    .style("opacity", 0);


    var canvasWidth = 800, 
      canvasHeight = 800,   
      outerRadius = 200,   
      color = d3.scale.category20(); 

      var legendRectSize = 18;
      var legendSpacing = 4;

      var div = d3.select("body").append("div")   
                  .attr("class", "tooltip1")               
                  .style("opacity", 0);

var tip1 = d3.tip()
  .attr('class', 'd3-tip1')
  .offset([70, 0])
  .html(function(d) {
    return "<strong>"+d.data.Title+"</strong> <span style='color:orange'>"+"$"+d.data.Worldwide+"M"+"</span>";
  });

d3.json("json/boxoffice"+iDyear+"_2mov.json", function(error, data){

  data.movies = data.movies.slice(from, to); // za 10 najboljih      

   x.domain(data.movies.map(function(d){ return d.Title}));
  y.domain([0, d3.max(data.movies, function(d){return d.Worldwide*1.05})]);

  console.log(data)
  var bar = chart.selectAll("g")
            .data(data.movies)
          .enter()
            .append("g")
            .attr("transform", function(d, i){
              return "translate("+x(d.Title)+", 0)";
            })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)
            .on('click', function(d){
                var active = d.active ? false:true,
                newOpacity = active ? 0.95:0;
                var MovieName = d.Title;
                var MovieWorldwide = d.Worldwide;
                var MovieDomestic = d.Domestic;
                var MovieOverseas = d.Overseas;
                var MovieRank = d.Rank;
                var MovieStudio = d.Studio;
                var MovieDomesticPer=d.Dper;
                var MovieOverseasPer=d.Oper;
                div .transition()
                    .duration(200)
                    .style("opacity", newOpacity);
                div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                d.active = active;
            });
  
  bar.call(tip);
  bar.append("rect")
      .attr("y", function(d) { 
        return y(d.Worldwide); 
      })
      .attr("x", function(d,i){
        return x.rangeBand()+(margin.left/2);
      })
      .attr("height", function(d) { 
        return height - y(d.Worldwide); 
      })
      .attr("width", x.rangeBand()*0.7) 
      .attr("transform", "translate(-35, 0)")

  bar.append("text")
      .attr("x", x.rangeBand()+margin.left )
      .attr("y", function(d) { return y(d.Worldwide) -10; })
      .attr("dy", ".75em");
  
  chart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate("+margin.left+","+ height+")")        
        .call(xAxis);
  
  chart.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate("+margin.left+",0)")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Boxoffice");

  d3.selectAll("g.x.axis g.tick line")
    .attr("y2", 25)
    .attr("x2", 0);   

/*pie chart */
    
    var vis = d3.select("#pie")
      .append("svg:svg") 
        .data([data.movies]) 
        .attr("width", canvasWidth) 
        .attr("height", canvasHeight) 
        .append("svg:g") 
          .attr("transform", "translate(" + 2.5*outerRadius + "," + 1.5*outerRadius + ")") 
vis.call(tip1);
    var arc = d3.svg.arc()
      .outerRadius(outerRadius);

    var pie = d3.layout.pie() 
      .value(function(d) { return d.Worldwide; }) 
      .sort( function(d) { return null; } );

    var arcs = vis.selectAll("g.slice")
      .data(pie)
      .enter()
      .append("svg:g")
      .attr("class", "slice");  

    arcs.append("svg:path")
      .attr("fill", function(d, i) { return color(i); } )
      .attr("d", arc)
      .on('mouseover', tip1.show)
      .on('mouseout', tip1.hide)
      .on('click', function(d){
                        var active = d.active ? false:true,
                        newOpacity = active ? 0.95:0;
                        var MovieName = d.data.Title;
                        var MovieWorldwide = d.data.Worldwide;
                        var MovieDomestic = d.data.Domestic;
                        var MovieOverseas = d.data.Overseas;
                        var MovieRank = d.data.Rank;
                        var MovieStudio = d.data.Studio;
                        var MovieDomesticPer=d.data.Dper;
                        var MovieOverseasPer=d.data.Oper;
                        div .transition()
                            .duration(200)
                            .style("opacity", newOpacity);
                        div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                        d.active = active;
                    });

  /*  arcs.append("svg:text")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius + 50; // Set Outer Coordinate
        d.innerRadius = outerRadius + 50; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" +angle(d) + ")";
      })
      .attr("text-anchor", "middle") 
      .style("fill", "Purple")
      .style("font", "bold 12px Arial");
      .attr("data-legend",function(d) { return d.data.Title; })
      .text(function(d, i) { return data.movies[i].Title; }); */

    arcs.filter(function(d) { return d.endAngle - d.startAngle > .2; }).append("svg:text")
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius; // Set Outer Coordinate
        d.innerRadius = outerRadius/2; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")";
      })
      .style("fill", "White")
      .style("font", "bold 12px Arial")
      .text(function(d) { return d.data.Worldwide; });

    

    var legend = vis.selectAll('.legend')
      .data(color.domain())
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', function(d, i) {
        var height = legendRectSize + legendSpacing;
        var offset =  height * color.domain().length / 2;
        var horz = -2 * legendRectSize-400;
        var vert = i * height - offset ;
        return 'translate(' + horz + ',' + vert + ')';
      });

    legend.append('rect')
    .attr('width', legendRectSize)
    .attr('height', legendRectSize)
    .style('fill', color)
    .style('stroke', color);

    legend.append('text')
      .attr('x', legendRectSize + legendSpacing)
      .attr('y', legendRectSize - legendSpacing)
      .text(function(d, i) { return data.movies[i].Title; });

    function angle(d) {
      var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
      return a > 90 ? a - 180 : a;
    }
});
}
else {
  alert("Unesite raspon od 10");
  document.getElementById("containForm").style.visibility = "visible";
    }
}
