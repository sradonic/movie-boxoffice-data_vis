var margin ={top:20, right:30, bottom:68, left:40},
    width=600-margin.left - margin.right, 
    height=650-margin.top-margin.bottom;

// scale to ordinal because x axis is not numerical
var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);

//scale to numerical value by height
var y = d3.scale.linear().range([height, 0]);
var val = 32;


  var div = d3.select("body").append("div") 
    .attr("class", "tooltip")       
    .style("opacity", 0);

      var canvasWidth = 800, 
      canvasHeight = 600,   
      outerRadius = 150,   
      color = d3.scale.category20(); 

      var legendRectSize = 18;
      var legendSpacing = 4;

      var div = d3.select("body").append("div")   
                  .attr("class", "tooltip1")               
                  .style("opacity", 0);

var tip1 = d3.tip()
  .attr('class', 'd3-tip1')
  .offset([70, 0])
  .html(function(d) {
    console.log(d);
    return "<strong>"+d.data.Title+"</strong> <span style='color:orange'>"+"$"+d.data.Worldwide+"M"+"</span>";
  });

  $.getJSON("json/boxoffice2015_2mov.json", function(data) 
    {
      //console.log(data.movies[0].Title);
      for (var i = 0 ; i < data.movies.length ; i++) 
            {
              var movie = data.movies[i]; 
              //console.log(i);
              //console.log(movie);
              //getMovie();
            var div_data='<option value="'+movie.Title+'">'+movie.Title+'</option>';
            //console.log(div_data);
            $(div_data).appendTo('.js-example-basic-multiple'); 
            //getMovie(movie);
            }  

    $(function() {
        $('#btn1').on('click', function() {
          $('div#chart').empty();
          $('div#pie').empty();
         //    $('#chart chart').remove();
          //   $('#chart').append('chart').attr('id', 'chart');
            // $('div#pie').remove();
        });
    });
 });   
            function DrawGraph() {
                        $('div#chart').empty();
          $('div#pie').empty();
        var select_value = $('.js-example-basic-multiple').val();
        console.log(select_value);
        if (select_value.length<4){
          alert("Unesite 5 filmova!")
        }
        else {
        sendStr(select_value);
      }
        $(".js-example-basic-multiple").select2("val", "All");
            }

function sendStr(selected_value){   
  d3.json("json/boxoffice2015_2mov.json", function(error, data){
    var moviesArray = [];
    for(var i = 0 ; i < data.movies.length ; i++) {
      for (var j = 0 ; j < selected_value.length ; j++) {
        if (data.movies[i].Title == selected_value[j]) moviesArray.push(data.movies[i]);
      }
    }

    data = moviesArray;

    console.log(data);

    var chart = d3.select("#chart")  
              .append("svg")  //append svg element inside #chart
              .attr("width", width+(2*margin.left)+margin.right)    //set width
              .attr("height", height+margin.top+margin.bottom);  //set height

var xAxis = d3.svg.axis()
              .scale(x)
              .orient("bottom");  //orient bottom because x-axis will appear below the bars

var yAxis = d3.svg.axis()
              .scale(y)
              .orient("left");

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>"+d.Title+"</strong> <span style='color:orange'>"+"$"+d.Worldwide+"M"+"</span>";
  });

    x.domain(data.map(function(d){ return d.Title}));
  y.domain([0, d3.max(data, function(d){return d.Worldwide*1.05})]);

  console.log(data)
  var bar = chart.selectAll("g")
            .data(data)
          .enter()
            .append("g")
            .attr("transform", function(d, i){
              return "translate("+x(d.Title)+", 0)";
            })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)
            .on('click', function(d){
                var active = d.active ? false:true,
                newOpacity = active ? 0.95:0;
                var MovieName = d.Title;
                var MovieWorldwide = d.Worldwide;
                var MovieDomestic = d.Domestic;
                var MovieOverseas = d.Overseas;
                var MovieRank = d.Rank;
                var MovieStudio = d.Studio;
                var MovieDomesticPer=d.Dper;
                var MovieOverseasPer=d.Oper;
                div .transition()
                    .duration(200)
                    .style("opacity", newOpacity);
                div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                d.active = active;
            });
  
  bar.call(tip);
  bar.append("rect")
      .attr("y", function(d) { 
        return y(d.Worldwide); 
      })
      .attr("x", function(d,i){
        return x.rangeBand()+(margin.left/2)+val;
      })
      .attr("height", function(d) { 
        return height - y(d.Worldwide); 
      })
      .attr("width", x.rangeBand()*0.7) 
      .attr("transform", "translate(-90, 0)")

  bar.append("text")
      .attr("x", x.rangeBand()+margin.left )
      .attr("y", function(d) { return y(d.Worldwide) -10; })
      .attr("dy", ".75em");
  
  chart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate("+margin.left+","+ height+")")        
        .call(xAxis);
  
  chart.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate("+margin.left+",0)")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Boxoffice");

  d3.selectAll("g.x.axis g.tick line")
    .attr("y2", 25)
    .attr("x2", 0); 


  var vis = d3.select("#pie")
      .append("svg:svg") 
        .data([data]) 
        .attr("width", canvasWidth) 
        .attr("height", canvasHeight) 
        .append("svg:g") 
          .attr("transform", "translate(" + 3.5*outerRadius + "," + 1.5*outerRadius + ")") 
vis.call(tip1);
    var arc = d3.svg.arc()
      .outerRadius(outerRadius);

    var pie = d3.layout.pie() 
      .value(function(d) { return d.Worldwide; }) 
      .sort( function(d) { return null; } );

    var arcs = vis.selectAll("g.slice")
      .data(pie)
      .enter()
      .append("svg:g")
      .attr("class", "slice");  

    arcs.append("svg:path")
      .attr("fill", function(d, i) { return color(i); } )
      .attr("d", arc)
      .on('mouseover', tip1.show)
      .on('mouseout', tip1.hide)
      .on('click', function(d){
                        var active = d.active ? false:true,
                        newOpacity = active ? 0.95:0;
                        var MovieName = d.data.Title;
                        var MovieWorldwide = d.data.Worldwide;
                        var MovieDomestic = d.data.Domestic;
                        var MovieOverseas = d.data.Overseas;
                        var MovieRank = d.data.Rank;
                        var MovieStudio = d.data.Studio;
                        var MovieDomesticPer=d.data.Dper;
                        var MovieOverseasPer=d.data.Oper;
                        div .transition()
                            .duration(200)
                            .style("opacity", newOpacity);
                        div .html(MovieName+"<br>"+"Rank: "+MovieRank+"<br>"+"Studio: "+MovieStudio+"<br>"+"Domestic Boxoffice: "+"$"+MovieDomestic+"M"+" ("+MovieDomesticPer+")"+"<br>"+"Overseas Boxoffice: "+"$"+MovieOverseas+"M"+" ("+MovieOverseasPer+")"+"<br>"+"Worldwide Boxoffice: "+"$"+MovieWorldwide+"M");
                        d.active = active;
                    });

  /*  arcs.append("svg:text")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius + 50; // Set Outer Coordinate
        d.innerRadius = outerRadius + 50; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" +angle(d) + ")";
      })
      .attr("text-anchor", "middle") 
      .style("fill", "Purple")
      .style("font", "bold 12px Arial");
      .attr("data-legend",function(d) { return d.data.Title; })
      .text(function(d, i) { return data.movies[i].Title; }); */

    arcs.filter(function(d) { return d.endAngle - d.startAngle > .2; }).append("svg:text")
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .attr("transform", function(d) { 
        d.outerRadius = outerRadius; // Set Outer Coordinate
        d.innerRadius = outerRadius/2; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")";
      })
      .style("fill", "White")
      .style("font", "bold 12px Arial")
      .text(function(d) { return d.data.Worldwide; });

    

    var legend = vis.selectAll('.legend')
      .data(color.domain())
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', function(d, i) {
        var height = legendRectSize + legendSpacing;
        var offset =  height * color.domain().length / 2;
        var horz = -2 * legendRectSize-400;
        var vert = i * height - offset;
        return 'translate(' + horz + ',' + vert + ')';
      });

    legend.append('rect')
    .attr('width', legendRectSize)
    .attr('height', legendRectSize)
    .style('fill', color)
    .style('stroke', color);

    legend.append('text')
      .attr('x', legendRectSize + legendSpacing)
      .attr('y', legendRectSize - legendSpacing)
      .text(function(d, i) { return data[i].Title; });

    function angle(d) {
      var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
      return a > 90 ? a - 180 : a;
    } 
});
}